import argparse
from simpleExtractor import extract_images

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("read_path", , nargs="?", default="", help="a path to a bag file or directory contianing bag files")
    ap.add_argument("write_path", nargs="?", default=None, help="location to save the images, default is read location")
    ap.add_argument("-f", "--image-format", default="jpg", help="Image format default jpg")
    args = ap.parse_args()
    extract_images(args.read_path, args.write_path, image_format=args.image_format)
