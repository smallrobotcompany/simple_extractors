!/bin/bash
mkdir --parents /home/$USER/.local/simple_extract/env/simple_extract
python3 -m venv /home/$USER/.local/simple_extract/env/simple_extract
source /home/$USER/.local/simple_extract/env/simple_extract/bin/activate
pip3 install --extra-index-url https://rospypi.github.io/simple/ rospy rosbag cv-bridge sensor-msgs geometry-msgs
pip3 install opencv-python
deactivate
git clone git@bitbucket.org:src_dolphin/simple_extractors.git /home/$USER/.local/simple_extract/src
echo "PATH=\$PATH:/home/\$USER/.local/simple_extract/src/cmds" >> /home/$USER/.bashrc
source /home/$USER/.bashrc