
import cv2
import rosbag
import os
import pathlib
from datetime import datetime, timedelta
from cv_bridge import CvBridge

def get_bags_paths(path) -> list:
    if os.path.isdir(path):
        bag_paths = sorted(["{}/{}".format(path, b) for b in os.listdir(path) if b.endswith(".bag")])
        if not len(bag_paths):
            raise OSError("No bag files in {}".format(path))
        else:
            return bag_paths
    elif path.lower().endswith(".bag"):
        return [path]
    else:
        raise OSError("Path is not a bag file or directory")


def rostime_to_iso(rostime):
    # converts rostime object to an isoformat string
    nsecs = rostime.to_nsec()
    dtime = datetime(1970, 1, 1) + timedelta(microseconds=nsecs//1000)
    return dtime.strftime("%Y-%m-%d-%H-%M-%S-%f")


def get_camera_serial(topic):
    if topic.startswith("/image_header"):
        topic = topic[13:]
    if(topic.split("/")[1] == "basler"):
        return str(topic.split("_")[-1])
    else:
        return topic.split("/")[1]

def clean_msg(msg):
    if hasattr(msg, "__slots__"):
        for sub in getattr(msg, "__slots__"):
            setattr(msg, sub, clean_msg(getattr(msg, sub)))
    elif type(msg) == map:
        msg = list(msg)
    if type(msg) == list:
        for sub in msg:
            clean_msg(sub)
    return msg

def make_read_path(path):
    path = os.path.expanduser(path)
    if path.endswith("/"):
        path = path[:-1]
    if not path.startswith("/"):
        path = "{}/{}".format(os.getcwd(), path)
    return path

def extract_images(read_path, write_path = None, image_format = "jpg", image_save_params = [1, 90]) -> None:
    read_path = make_read_path(read_path)
    bag_paths = get_bags_paths(read_path)
    write_path = '{}/images'.format("/".join(read_path.split("/")[:-1])) if write_path is None else write_path
    
    if write_path.lower().endswith(".bag"):
        write_name = write_path.split("/")[-1]
        write_loc = "/".join(write_path.split("/")[:-1])
    else:
        try:
            write_name = '{}_data.bag'.format(read_bag_paths[0].split("/")[-1].split(".")[0][:20])
        except:
            write_name = "data.bag"
        write_loc = write_path
    bridge = CvBridge()

    pathlib.Path(write_path).mkdir(parents=True, exist_ok=True)
    for p in bag_paths:
        bag = rosbag.Bag(p)
        for topic, msg, time in bag.read_messages():
            if msg._type == "sensor_msgs/Image":
                img_path = "{}/{}_{}.{}".format(write_path, get_camera_serial(topic), rostime_to_iso(time),  image_format)
                cv2.imwrite(img_path, bridge.imgmsg_to_cv2(
                    msg, desired_encoding='passthrough'), image_save_params)
        bag.close()


def extract_data(read_path, write_path=None, ) -> None:
    read_path = make_read_path(read_path)
    write_path = read_path if write_path is None else os.path.expanduser(write_path)
    read_bag_paths = get_bags_paths(read_path)
    if write_path.lower().endswith(".bag"):
        write_name = write_path.split("/")[-1]
        write_loc = "/".join(write_path.split("/")[:-1])
    else:
        try:
            write_name = '{}_data.bag'.format(read_bag_paths[0].split("/")[-1].split(".")[0][:20])
        except:
            write_name = "data.bag"
        write_loc = write_path

    
    pathlib.Path(write_loc).mkdir(parents=True, exist_ok=True)
    write_bag = rosbag.Bag("{}/{}".format(write_loc, write_name), "w")
    for read_bag_path in read_bag_paths:
        try:
            read_bag = rosbag.Bag(read_bag_path)
        except:
            print("Error reading Bag {}: Skipped".format(read_bag_path))
            continue
        for topic, msg, time in read_bag.read_messages():
            if msg._type == "sensor_msgs/Image":
                msg = msg.header
                topic = "/image_header" + topic
            
            try:
                write_bag.write(topic, msg, time)
            except:
                write_bag.write(topic, clean_msg(msg), time)
        read_bag.close()
    write_bag.close()
