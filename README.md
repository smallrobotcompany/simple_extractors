# Extract or remove images from a ros bag

Requires:
ros-rosbag
cv_bridge

Use:
```python
extract_data(read_path, write_path=None)
# Extracts the data (without images) from a bag or directory of bags

# Inputs:
# read_path (str or list): the location of a .bag file or a directory containing bag files
# write_path (str opional): A directory or file path where the output bag will be saved (will defult to the read_path)

extract_images(read_path, write_path=None)
# Extracts the images from a bag or directory of bags

# Inputs:
# read_path (str or list): the location of a .bag file or a directory containing bag files
# write_path (str opional): A directory path to save the images
```