import argparse
from simpleExtractor import extract_data

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("read_path", nargs="?", default="", help="a path to a bag file or directory contianing bag files")
    ap.add_argument("write_path", nargs="?", default=None, help="location to save the data bag, default is read location")
    args = ap.parse_args()
    extract_data(args.read_path, args.write_path)
